import React, { useState, useEffect, createContext } from 'react';

export const BooksContext = createContext();

export const BooksProvider = props => {

  const [books] = useState(() => {

    const localData = localStorage.getItem('books');

    return localData ? JSON.parse(localData) : [];
  });

  useEffect(() => {

    const fetchData = async () => {

      try {

        const response = await fetch(
          'https://www.googleapis.com/books/v1/volumes?q=HARRY%20POTTER'
        );
        const data = await response.json();

        localStorage.setItem('books', JSON.stringify(data.items));

      } catch (err) {

        console.log(err);
      }
    }

    fetchData();

  }, []);

  return (
    <BooksContext.Provider 
      value={[books]}
    >
      { props.children }
    </BooksContext.Provider>
  )
};