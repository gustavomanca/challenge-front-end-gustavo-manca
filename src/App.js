import React from 'react';
import { Helmet } from 'react-helmet';

import { BooksProvider } from './context/books-context';

import { GlobalStyle } from './styles';
import { Hero, Books, Footer } from './components';

function App() {

  return (
    <>
      <Helmet>
        <title>Pixter Books</title>
        <link rel="icon" type="image/png" href="favicon.png" sizes="16x16" />
      </Helmet>
      <BooksProvider>
        <GlobalStyle /> 
        <Hero />
        <Books />
        <Footer />
      </BooksProvider>
    </>
  );
}

export default App;
