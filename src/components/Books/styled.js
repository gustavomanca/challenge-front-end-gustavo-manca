import styled from 'styled-components';
import { Colors } from '../../styles';

export const Wrapper = styled.section`

  background-color: ${Colors.white};
  overflow: ${props => props.overflow};
  padding: 6.4rem 3.2rem;

  .description {
    font-size: 2rem;
    margin-bottom: 6.5rem;
    text-align: center;
  }
  
  @media (min-width: 992px) {
    padding: 12rem 31rem 6rem;
  }
`;

export const Title = styled.h2`

  font-size: 3.2rem;
  font-weight: bold;
  margin-bottom: 2.5rem;
  text-align: center;
`;

export const BooksWrapper = styled.div`

  display: flex;
  flex-wrap: wrap;
  justify-content: center;

  > .slick-slider {
    height: 35rem;
    width: 20rem;

    > .slick-list {
      height: 100%;
    }

    .slick-dots {
      bottom: -6rem;
      margin: 1.5rem 0;
    }

    .slick-dots li {
      margin: 0;
    }
  }

  @media (min-width: 992px) {

    display: grid;
    grid-template-columns: repeat(4, 13rem);
    justify-content: space-between;
    grid-row-gap: 2rem;
  }
`;

export const BookContainer = styled.div`

  background-image: url(${props => props.path});
  background-position: center;
  background-size: cover;
  box-shadow: .1rem .1rem 1rem rgba(0,0,0, 0.2);
  height: 35rem;
  overflow: hidden;
  position: relative;
  transition: 
    box-shadow .1s, 
    transform .1s;
  will-change: box-shadow, transform;
  width: 100%;

  &:not(:last-child) {
    margin-bottom: 3rem;
  }

  &:hover {
    box-shadow: .2rem .2rem 1rem rgba(0,0,0, 0.3);
    cursor: pointer;
    transform: translate(-.2rem, -.2rem);
    transition: 
      box-shadow .5s ease-out,
      transform .3s ease-out;
    will-change: box-shadow, transform;
  }

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,0.6);
    opacity: 0;
    transition: opacity .4s ease;
  }

  &:hover::before {
    opacity: 1;
    transition: opacity .6s ease-out;
  }

  &:hover::after {
    content: 'More...';
    color: #fff;
    font-size: 2rem;
    left: 30%;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    width: 2rem;
  }

  > img {
    position: absolute;
    visibility: hidden;
  }
  
  @media (min-width: 992px) {

    height: 20rem;
    width: 100%;

    &:not(:last-child) {
      margin-bottom: unset;
    }
  }
`;

export const ModalOverlay = styled.div`

    background-color: rgba(0,0,0, 0.8);
    height: 100vh;
    left: 0;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 100;
`;