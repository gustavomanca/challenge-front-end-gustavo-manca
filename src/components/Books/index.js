import React, { useContext, useState } from 'react';

import { isMobile } from "react-device-detect";

import { BooksContext } from '../../context/books-context';

import { BookModal, SliderWrapper, CarouselContainer } from '../';

import {
  Wrapper,
  Title,
  BooksWrapper,
  BookContainer,
  ModalOverlay
} from './styled';

const Books = () => {

  const [books] = useContext(BooksContext);
  const [showModal, setShowModal] = useState(false);
  const [activeBook, setActiveBook] = useState(undefined);

  const handleBookModal = bookIndex => {

    setShowModal(true);
    setActiveBook(bookIndex);
  };

  const onCloseModal = () => {
    setShowModal(false);
  }

  return (

    <Wrapper 
      name="books" 
      overflow={
        showModal ? 'hidden' : 'visible'
      }
    >

      <Title>
        Books
      </Title>

      <p className="description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae 
        eros eget tellus tristique bibendum. Donec rutum sed sem quis venenatis.
      </p>

      <BooksWrapper>

        { isMobile 
        
          ?
            <SliderWrapper>

              {books.map((book, index) => (
                <div key={book.id}>
                  <BookContainer 
                    path={book.volumeInfo.imageLinks.thumbnail}
                    onClick={() => handleBookModal(index)}
                  >
                    <img 
                      src={book.volumeInfo.imageLinks.thumbnail} 
                      alt={book.volumeInfo.title} 
                    />
                  </BookContainer>
                </div>
              ))}

            </SliderWrapper>

          :
            books.map((book, index) => (
              <div key={book.id}>
                <BookContainer 
                  path={book.volumeInfo.imageLinks.thumbnail}
                  onClick={() => handleBookModal(index)}
                >
                  <img 
                    src={book.volumeInfo.imageLinks.thumbnail} 
                    alt={book.volumeInfo.title} 
                  />
                </BookContainer>
              </div>
            ))
        }
        

      </BooksWrapper>

      { !!showModal && (
        <>
          <BookModal book={ books[activeBook] } onCloseModal={ onCloseModal } /> 
          <ModalOverlay />
        </>
      )}

    </Wrapper>
  )
}

export default Books;