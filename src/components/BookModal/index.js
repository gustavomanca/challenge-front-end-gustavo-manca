import React from 'react';

import { isMobile } from "react-device-detect";

import { 
  Modal, 
  Close, 
  Title, 
  Subtitle,
  Authors, 
  DescriptionWrapper, 
  Thumbnail,
  Description
} from './styled';

const BookModal = props => {

  const { book, onCloseModal } = props;
  const { 
    title, 
    subtitle, 
    authors, 
    description, 
    imageLinks: { 
      smallThumbnail 
    } 
  } = book.volumeInfo;

  console.log(book);

  return (

    <Modal height={ isMobile ? '100vh' : 'auto' }>

      <Close onClick={onCloseModal} />
      <Title>
        {title}
      </Title>
      <Subtitle>
        {subtitle}
      </Subtitle>
      <Authors>
        Authors: {authors.join(', ')}
      </Authors>

      <DescriptionWrapper>
        <Thumbnail src={smallThumbnail} />
        <Description>
          {description}
        </Description>
      </DescriptionWrapper>
    </Modal>
  )
}

export default BookModal;