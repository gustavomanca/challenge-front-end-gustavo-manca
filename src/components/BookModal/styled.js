import styled from 'styled-components';
import { Colors } from '../../styles';

export const Modal = styled.div`

  background-color: ${Colors.white};
  border-radius: 1rem;
  height: ${props => props.height};
  left: 50%;
  padding: 4rem 2rem;
  position: fixed;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 42rem;
  z-index: 200;
`;

export const Close = styled.button`

  background: none;
  height: 4.8rem;
  outline: none;
  position: absolute;
  right: 0;
  top: 0;
  width: 4.8rem;

  &:hover {
    cursor: pointer;
    opacity: .8;
    transition: opacity .3s ease;
    will-change: opacity;
  }

  &::before,
  &::after {
    background-color: ${Colors.black};
    content: '';
    height: .3rem;
    left: 50%;
    position: absolute;
    top: 50%;
    width: 2rem;
  }

  &::before {
    transform: 
      translate(-50%, -50%)
      rotate(45deg)
  }

  &::after {
    transform: 
      translate(-50%, -50%)
      rotate(135deg)
  }
`;

export const Title = styled.h2`

  font-size: 2rem;
  font-weight: bold;
  margin-bottom: .4rem;
  text-align: center;
`;

export const Subtitle = styled.h3`

  font-size: 1.6rem;
  margin-bottom: .8rem;
  text-align: center;
`;

export const Authors = styled.span`

  display: block;
  font-size: 1.2rem;
  margin-bottom: 1.6rem;
  text-align: center;
`;

export const DescriptionWrapper = styled.div`

`;

export const Thumbnail = styled.img`

  float: left;
  margin: 0 1rem 1rem 0;
  max-height: 12rem;
  width: 8rem;
`;

export const Description = styled.p`

  font-size: 1.2rem;
  line-height: 1.2;
`;