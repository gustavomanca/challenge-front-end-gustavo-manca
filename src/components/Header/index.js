import React from 'react'

import { Link } from 'react-scroll';

import brandLogo from '../../assets/images/logo-pixter.png';

import { 
  HeaderWrapper,
  PixterLink,
  Logo,
  NavBar,
  NavList,
  NavItem,
} from './styled';

const Header = () => {

  return (
    <HeaderWrapper> 
      <PixterLink 
        href="https://pixter.com.br/" 
        target="_blank" 
        rel="noopener noreferrer nofollow"
        title="Go to the Pixter Website"
      >
        <Logo src={ brandLogo } alt="Pixter" />
      </PixterLink>
      <NavBar>
        <NavList>
          <NavItem className="nav-item">
            <Link to="books" smooth={true} className="nav-link">
              Books
            </Link>
          </NavItem>
          <NavItem className="nav-item">
            <Link to="newsletter" smooth={true} className="nav-link">
              Newsletter
            </Link>
          </NavItem>
          <NavItem className="nav-item">
            <Link to="address" smooth={true} className="nav-link">
              Address
            </Link>
          </NavItem>
        </NavList>
      </NavBar>
    </HeaderWrapper>
  )
}

export default Header;