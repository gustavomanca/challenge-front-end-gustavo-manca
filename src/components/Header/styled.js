import styled from 'styled-components';
import { Colors, Common } from '../../styles';

const { onHoverOpacity } = Common;

export const HeaderWrapper = styled.header`

  align-items: center;
  display: flex;
  justify-content: space-between;
  margin-bottom: 7rem;
`;

export const PixterLink = styled.a`

  @media (min-width: 992px) {

    &:hover {
      ${onHoverOpacity}
    }
  }
`;

export const Logo = styled.img`

  width: 8rem;

  @media (min-width: 992px) {

    width: 10rem;
  }
`;

export const NavBar = styled.nav``;

export const NavList = styled.ul`

  display: none;

  @media (min-width: 992px) {
    display: flex;
  }
`;

export const NavItem = styled.li`

  margin-right: 8rem;

  > .nav-link {

    color: ${ Colors.black };
    font-size: 2.2rem;
    font-weight: bold;

    @media (min-width: 992px) {
      
      &:hover {
        ${onHoverOpacity}
      }
    }
  }
`;