export { default as FacebookIcon } from './Facebook';
export { default as TwitterIcon } from './Twitter';
export { default as GooglePlusIcon } from './GooglePlus';
export { default as PinterestIcon } from './Pinterest';
export { default as IconWrapper } from './IconWrapper';