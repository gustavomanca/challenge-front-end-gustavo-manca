import styled from 'styled-components';
import { Colors } from '../../../styles';

import { ScFacebook as Facebook } from 'styled-icons/evil/ScFacebook';

export const FacebookBrand = styled(Facebook)`

  bottom: -.8rem;
  color: ${props => props.color || Colors.black };
  height: 6rem;
  position: absolute;
  width: 6rem;

  @media (min-width: 992px) {
    height: 5rem;
    width: 5rem;
  }
`;