import React from 'react';

import { IconWrapper } from '../index';

import { FacebookBrand } from './styled';

const FacebookIcon = () => {

  return (
    <IconWrapper>
      <FacebookBrand />
    </IconWrapper>
  )
}

export default FacebookIcon;