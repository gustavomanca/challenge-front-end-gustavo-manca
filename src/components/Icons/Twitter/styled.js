import styled from 'styled-components';
import { Colors } from '../../../styles';

import { Twitter } from 'styled-icons/boxicons-logos/Twitter';

export const TwitterBrand = styled(Twitter)`

  bottom: 50%;
  color: ${props => props.color || Colors.black };
  height: 3rem;
  left: 50%;
  position: absolute;
  transform: translate(-50%, 50%);
  width: 3rem;
`;