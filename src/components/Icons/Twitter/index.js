import React from 'react';

import { IconWrapper } from '../index';

import { TwitterBrand } from './styled';

const FacebookIcon = () => {

  return (
    <IconWrapper>
      <TwitterBrand />
    </IconWrapper>
  )
}

export default FacebookIcon;