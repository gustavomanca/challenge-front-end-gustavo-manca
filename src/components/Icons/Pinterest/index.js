import React from 'react';

import { IconWrapper } from '../index';

import { PinterestBrand } from './styled';

const PinterestIcon = () => {

  return (
    <IconWrapper>
      <PinterestBrand />
    </IconWrapper>
  )
}

export default PinterestIcon;