import styled from 'styled-components';
import { Colors } from '../../../styles';

import { PinterestAlt as Pinterest } from 'styled-icons/boxicons-logos/PinterestAlt';

export const PinterestBrand = styled(Pinterest)`

  color: ${props => props.color || Colors.black };
  height: 5rem;
  width: 5rem;

  @media (min-width: 992px) {
    height: 4rem;
    width: 4rem;
  }
`;