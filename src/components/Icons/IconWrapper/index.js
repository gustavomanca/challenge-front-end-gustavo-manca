import React from 'react';

import { IconWrapperShape } from './styled';

const IconWrapper = props => (

    <IconWrapperShape>
      { props.children }
    </IconWrapperShape>
);

export default IconWrapper;