import styled from 'styled-components';
import { Colors } from '../../../styles';

export const IconWrapperShape = styled.div`

  background-color: ${ props => props.bgColor || Colors.yellow };
  border-radius: .6rem;
  position: relative;
  height: 4.8rem;
  width: 4.8rem;

  @media (min-width: 992px) {
    height: 3.6rem;
    width: 3.6rem;
  }
`;