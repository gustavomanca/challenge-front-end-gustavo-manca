import React from 'react';

import { IconWrapper } from '../index';

import { GooglePlusBrand } from './styled';

const GooglePlusIcon = () => {

  return (
    <IconWrapper>
      <GooglePlusBrand />
    </IconWrapper>
  )
}

export default GooglePlusIcon;