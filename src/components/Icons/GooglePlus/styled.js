import styled from 'styled-components';
import { Colors } from '../../../styles';

import { SocialGooglePlus as GooglePlus } from 'styled-icons/typicons/SocialGooglePlus';

export const GooglePlusBrand = styled(GooglePlus)`

  bottom: 50%;
  color: ${props => props.color || Colors.black };
  height: 5rem;
  left: 50%;
  position: absolute;
  transform: translate(-50%, 50%);
  width: 5rem;  
`;