import React, { Component } from 'react';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

const CarouselContainer = ({children}) => {

  return (
    <Carousel>
      {children}
    </Carousel>
  );
}

export default CarouselContainer;