import React from 'react';

import brandLogo from '../../assets/images/logo-pixter.png';
import fingerPrintLogo from '../../assets/images/logo-fingerprint.png';

import { LogoWrapper } from './styled';

const BrandLogo = () => {

  return (
    <LogoWrapper>
      <img src={ brandLogo } alt="Pixter" />
      <img src={ fingerPrintLogo } alt="Pixter Finger Print" />
    </LogoWrapper>
  )

};

export default BrandLogo;