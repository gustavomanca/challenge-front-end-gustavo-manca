import styled from 'styled-components';

export const LogoWrapper = styled.div`

  display: flex;
  max-height: 10rem;
  width: 20rem;

  > img {
    width: 100%;
  }
`;