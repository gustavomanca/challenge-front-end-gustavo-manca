import React, { useState } from 'react';

import { 
  FooterSection,
  Wrapper,
  Title,
  Subtitle,
  Form,
  Input,
  Button,
  SocialMediaWrapper,
  List,
  Item,
  LocatedAtWrapper,
  LocatedAtList,
  LocatedAtItem,
  Text,
  Message
} from './styled';

import { 
  FacebookIcon,
  TwitterIcon,
  GooglePlusIcon,
  PinterestIcon
} from '../Icons';

const Footer = () => {

  const [email, setEmail] = useState();
  const [message, setMessage] = useState(false);

  const handleSubmit = e => {

    e.preventDefault();

    setMessage(true);

    setTimeout(() => {
      setMessage(false)
      setEmail('')
    }, 5000);

    console.log(document.title)
    console.log(email)
  }

  return (

    <FooterSection name="newsletter">

      <Wrapper>

        <Title>
          Keep in touch with us
        </Title>
        <Subtitle>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent 
          vitae eros eget tellus tristique bibendum. Donec rutum sed sem quis 
          venenatis.
        </Subtitle>
        <Form method="POST" onSubmit={ handleSubmit }>
          <Input 
            type="email"
            name="email"
            value={email}
            className="newsletter-input" 
            placeholder="Enter your email to update"
            onChange={ e => setEmail(e.target.value) }
          />
          <Button 
            type="submit" 
            className="newsletter-button"
          >
            Submit
          </Button>

          {!!message && 
            <Message>
              You've been subscribed successfully!
            </Message>
          }
        </Form>

        <SocialMediaWrapper>
          <List>
            <Item>
              <a 
                href="https://www.facebook.com/pixtertecnologia" 
                target="_blank" 
                rel="noopener noreferrer nofollow" 
                title="Pixter Facebook"
              >
                <FacebookIcon />
              </a>
            </Item>
            <Item>
              <a 
                href="https://www.twitter.com/" 
                target="_blank" 
                rel="noopener noreferrer nofollow" 
                title="Pixter Twitter"
              >
                <TwitterIcon />
              </a>
            </Item>
            <Item>
              <a 
                href="https://www.google.com/" 
                target="_blank" 
                rel="noopener noreferrer nofollow" 
                title="Pixter Plus+"
              >
                <GooglePlusIcon />
              </a>
            </Item>
            <Item>
              <a 
                href="https://www.pinterest.com/" 
                target="_blank" 
                rel="noopener noreferrer nofollow" 
                title="Pixter Pinterest"
              >
                <PinterestIcon />
              </a>
            </Item>
          </List>
        </SocialMediaWrapper>

        <LocatedAtWrapper name="address">
          <LocatedAtList>
            <LocatedAtItem>
              <Text>
                Alameda Santos, 1978
                6th floor - Jardim Paulista
              </Text>
              <Text>
                São Paulo - SP
              </Text>
              <Text>
                +55 11 3090 8500
              </Text>
            </LocatedAtItem>
            <LocatedAtItem>
              <Text>
                London - UK
              </Text>
              <Text>
                125 Kingsway
              </Text>
              <Text>
                London WC2B 6NH
              </Text>
            </LocatedAtItem>
            <LocatedAtItem>
              <Text>
                Lisbon - Portugal
              </Text>
              <Text>
                Rua Rodrigues Faria, 103
                4th floor
              </Text>
              <Text>
                Lisbon - Portugal
              </Text>
            </LocatedAtItem>
            <LocatedAtItem>
              <Text>
                Curitiba - PR
              </Text>
              <Text>
                R. Francisco Rocha, 198
              </Text>
              <Text>
                Batel - Curitiba - PR
              </Text>
            </LocatedAtItem>
            <LocatedAtItem>
              <Text>
                Buenos Aires - Argentina
              </Text>
              <Text>
                Esmeralda 950
              </Text>
              <Text>
                Buenos Aires B C1007
              </Text>
            </LocatedAtItem>
          </LocatedAtList>
        </LocatedAtWrapper>

      </Wrapper>
    </FooterSection>
  )
}

export default Footer;