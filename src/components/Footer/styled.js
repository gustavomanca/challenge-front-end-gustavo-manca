import styled from 'styled-components';
import { Colors, Common } from '../../styles';

const { onHoverOpacity } = Common;

export const FooterSection = styled.footer`

  background-color: ${Colors.black};
`;

export const Wrapper = styled.div`
  
  align-items: center;
  display: flex;
  flex-direction: column;
  padding: 6.4rem 3.2rem;

  @media (min-width: 992px) {
    padding: 10rem 22rem 17rem;
  }
`;

export const Title = styled.h2`

  color: ${Colors.yellow};
  font-size: 3.2rem;
  font-weight: bold;
  margin-bottom: 2.5rem;
  text-align: center;
`;

export const Subtitle = styled.p`

  color: ${Colors.gray};
  font-size: 2rem;
  margin-bottom: 4rem;
  text-align: center;
`;

export const Form = styled.form`

  display: flex;
  flex-direction: column;
  margin-bottom: 8rem;
  position: relative;
  width: 100%;

  @media (min-width: 992px) {
    flex-direction: row;
    width: unset;
  }
`;

export const Input = styled.input`

  border-radius: .4rem;
  height: 5rem;
  font-size: 1.6rem;
  margin-bottom: 2.4rem;
  padding-left: 1.6rem;

  @media (min-width: 992px) {
    font-size: 2rem;
    margin-bottom: unset;
    margin-right: 2rem;
    padding-left: 2.4rem;
    width: 58rem;
  }
`;

export const Button = styled.button`

  border-radius: .4rem;
  background-color: ${Colors.yellow};
  font-size: 1.6rem;
  font-weight: bold;
  height: 5rem;
  margin-bottom: 2rem;
  text-transform: uppercase;
  width: 100%;

  @media (min-width: 992px) {

    margin-bottom: unset;
    width: 12.6rem;

    &:hover {
      ${onHoverOpacity}
    }
  }
`;

export const Message = styled.span`

    bottom: -1.6rem;
    color: white;
    display: block;
    font-size: 1.6rem;
    left: 0;
    position: absolute;

    @media (min-width: 992px) {
      bottom: -3.6rem;
      left: 50%;
      text-align: center;
      transform: translateX(-50%);
    }
`;

export const SocialMediaWrapper = styled.div`

  display: flex;
  flex-direction: column;
  margin-bottom: 8rem;
  width: 100%;

  @media (min-width: 992px) {
    margin-bottom: 13.5rem;
    width: unset;
  }
`;

export const List = styled.ul`

  display: flex;
  justify-content: space-between;
  width: 100%;

  @media (min-width: 992px) {
    width: unset;
  }
`;

export const Item = styled.li`

  &:not(:last-child) {
    margin-right: 3rem;
  }

  @media (min-width: 992px) {

    transition: transform .2s ease-out;
    will-change: transform;

    &:hover {
      ${onHoverOpacity};
      transform: translate(-.1rem, -.1rem);
      transition: transform .4s ease-out;
      will-change: transform;
    }
  }
`;

export const LocatedAtWrapper = styled.div`

  width: 100%;
`;

export const LocatedAtList = styled.ul`

  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media (min-width: 992px) {
    flex-direction: row;
  }
`;

export const LocatedAtItem = styled.li`

  color: ${Colors.white};
  padding-bottom: 1.6rem;

  &:not(:last-child) {
    border-bottom: .1rem solid rgba(255, 255, 255, 0.4);
    margin-bottom: 1.6rem;
  }

  @media (min-width: 992px) {

    max-width: 17rem;
    padding-bottom: unset;

    &:not(:last-child) {
      border-bottom: none;
      margin-bottom: unset;
    }
  }
`;

export const Text = styled.span`

  display: block;
  font-size: 1.6rem;
  line-height: 1.3;
`;
