import React from 'react';

import { Header } from '../';

import { 
  Wrapper, 
  Content, 
  AppleBrand, 
  AndroidBrand, 
  WindowsBrand, 
  Tablet 
} from './styled';

const Hero = () => {

  return (
    <Wrapper>
      <Header />
      <Content>
        <div className="text-wrapper">
          <h1 className="title">
            Pixter Digital Books
          </h1>
          <h2 className="subtitle">
            Lorem ipsum dolor sit amet? <br />
            consectetur elit, volutpat.
          </h2>
          <p className="description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent vitae eros eget tellus tristique bibendum. Donec 
            rutrum sed sem quis venenatis. Proin viverra risus a eros
            volutpat tempor. In quis arcu et eros porta lobortis sit.
          </p>
          <div className="operational-systems">
            <AppleBrand />
            <AndroidBrand />
            <WindowsBrand />
          </div>
        </div>
        <div className="device-wrapper">
          <Tablet/>
        </div>
      </Content>
    </Wrapper>
  )
}

export default Hero;