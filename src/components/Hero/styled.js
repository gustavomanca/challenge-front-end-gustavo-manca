import styled from 'styled-components';
import { Colors } from '../../styles';

import { Apple } from 'styled-icons/fa-brands/Apple';
import { Android } from 'styled-icons/material/Android';
import { Windows } from 'styled-icons/fa-brands/Windows';

export const Wrapper = styled.section`

  background-color: ${Colors.yellow};
  padding: 6.4rem 3.2rem;

  @media (min-width: 992px) {
    height: 74rem;
    padding: 5.8rem 23.8rem;
  }
`;

export const Header = styled.header`

  align-items: center;
  display: flex;
  justify-content: space-between;
  margin-bottom: 7rem;

  .logo { 
    width: 8rem;
  } 

  @media (min-width: 992px) {

    .logo { 
      width: 10rem;
    } 
  }
`;

export const NavList = styled.ul`

  display: none;

  .nav-item:not(:last-child) {
    margin-right: 8rem;
  }

  .nav-link {
    color: ${ Colors.black };
    font-size: 2.2rem;
    font-weight: bold;
  }

  @media (min-width: 992px) {
    display: flex;
  }
`;

export const Content = styled.div`

  display: flex;

  .text-wrapper {
    width: 57.4rem;
  }

  .title {
    font-size: 3.2rem;
    margin-bottom: 3rem;
  }

  .subtitle {
    font-size: 2rem;
    font-weight: regular;
    margin-bottom: 3rem;
  }

  .description {
    font-size: 1.6rem;
    margin-bottom: 4rem;
    max-width: 40rem;
  }

  .operational-systems {
    align-items: baseline;
    display: flex;

    > *:not(:last-child) {
      margin-right: 3.5rem;
    }
  }

  @media (min-width: 992px) {

    .text-wrapper {
      padding-top: 7rem;
    }
  }

`;

export const AppleBrand = styled(Apple)`

  color: ${Colors.black};
  width: 3.5rem;
`;

export const AndroidBrand = styled(Android)`
  
  color: ${Colors.black};
  width: 3.5rem;
`;

export const WindowsBrand = styled(Windows)`
  
  color: ${Colors.black};
  width: 3.5rem;
`;

export const Tablet = styled.div`

  display: none;

  background-color: ${Colors.white};
  border-radius: 2rem;
  height: 47rem;
  position: relative;
  padding: 5rem 1.5rem;
  width: 32rem;

  &::after {
    background-color: ${Colors.black};
    content: '';
    display: block;
    width: 100%;
    height: 100%;
  }

  @media (min-width: 992px) {
    display: block;
  }

`;