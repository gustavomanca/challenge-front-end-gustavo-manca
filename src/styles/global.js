import { createGlobalStyle } from 'styled-components';
import Colors from './colors';

export default createGlobalStyle`

  @import url('https://fonts.googleapis.com/css?family=PT+Sans:400,700&display=swap');

  html, 
  body,
  div,
  input,
  button,
  ul,
  h1,
  h2,
  h3,
  p {
    margin: 0;
    padding: 0;
    border: 0;
    vertical-align: baseline;
  }

  html, body {
    color: ${Colors.black};
    font-family: 'PT Sans', sans-serif;
    font-size: 10px;
    line-height: 1;
  }

  ol,
  ul {
    list-style: none;
    padding-inline-start: 0;
  }

  a,
  a:active,
  a:visited {
    text-decoration: none;
  }

  h1 {
    font-weight: bold;
  }

  *::before,
  *,
  *::after {
    box-sizing: border-box;
    font-family: 'PT Sans', sans-serif;
    font-size: 10px;
    font-stretch: normal;
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
  }
  
`;