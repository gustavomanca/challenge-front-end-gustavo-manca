const Colors = {
  "black": '#010101',
  "gray": '#717171',
  "yellow": '#FCDB00',
  "white": '#fff'
}

export default Colors;