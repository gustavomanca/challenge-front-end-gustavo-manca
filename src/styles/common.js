import { css } from 'styled-components';

const onHoverOpacity = css`
  cursor: pointer;
  opacity: .8;
  transition: opacity .3s ease;
  will-change: opacity;
`;

export default { onHoverOpacity };